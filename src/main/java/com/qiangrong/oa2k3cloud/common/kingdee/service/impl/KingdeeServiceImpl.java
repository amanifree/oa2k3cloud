package com.qiangrong.oa2k3cloud.common.kingdee.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.qiangrong.oa2k3cloud.common.config.K3CloundConfig;
import com.qiangrong.oa2k3cloud.common.kingdee.service.KingdeeService;
import com.qiangrong.oa2k3cloud.common.utils.http.HttpUtil;
import com.qiangrong.oa2k3cloud.common.utils.result.ResultUtil;
import com.qiangrong.oa2k3cloud.common.vo.ResultVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @Description: 金蝶物料接口实现类
 * @date 2019/1/4 14:24
 */
@Service
@Slf4j
public class KingdeeServiceImpl implements KingdeeService {

    @Autowired
    private K3CloundConfig k3CloundConfig;

    @Override
    public ResultVo login(String url,String content) {

        ResponseEntity<String> responseEntity = HttpUtil.httpPost(url, content);
        //获取登录cookie
        if(responseEntity.getStatusCode()==HttpStatus.OK){
            String login_cookie = "";
            Set<String> keys = responseEntity.getHeaders().keySet();
            for(String key:keys){
                if (key.equalsIgnoreCase("Set-Cookie")) {
                    List<String> cookies = responseEntity.getHeaders().get(key);
                    for(String cookie:cookies){
                        if(cookie.startsWith("kdservice-sessionid")){
                            login_cookie=cookie;
                            break;
                        }
                    }
                }
            }
            Map<String,Object> map = new HashMap<>();
            map.put("cookie",login_cookie);
            return ResultUtil.success(map);
        }

        Map<String,Object> result = JSON.parseObject(responseEntity.getBody());
        return ResultUtil.error(result.get("Message").toString());
    }

    @Override
    public ResultVo view(String cookie, String content) {
        return null;
    }

    @Override
    public ResultVo save(String url,String cookie, String content) {
        //保存
        Map<String,Object> header = new HashMap<>();
        header.put("Cookie",cookie);
        String result = HttpUtil.httpPost(url,header,content);
        JSONObject jsonObject = JSON.parseObject(result);
        Map<String,Object> map = (Map<String, Object>) jsonObject.get("Result");
        Map<String,Object> responseStatus = (Map<String, Object>) map.get("ResponseStatus");
        Boolean isSuccess = (Boolean) responseStatus.get("IsSuccess");
        if(isSuccess){
            return ResultUtil.success(map);
        }else{
            List<Map<String,Object>> errors = (List<Map<String, Object>>) responseStatus.get("Errors");
            return ResultUtil.error(JSON.toJSONString(errors));
        }
    }

}
