package com.qiangrong.oa2k3cloud.common.kingdee.service;

import com.qiangrong.oa2k3cloud.common.vo.ResultVo;

/**
 * @Description: 金蝶接口服务
 * @date 2019/1/4 14:09
 */
public interface KingdeeService {

    /**
     * @Description: 金蝶登录接口
     * @param url 接口地址
     * @param content 查询参数
     * @date 2019/1/4 14:12
     */
    ResultVo login(String url, String content);


    /**
     * @Description: 金蝶查询接口
     * @param cookie 登录cookie
     * @param content 查询参数
     * @date 2019/1/7 17:21
     */
    ResultVo view(String cookie,String content);

    /**
     * @Description: 保存接口
     * @param url 接口地址
     * @param cookie 登录cookie
     * @param content json格式参数
     * @date 2019/1/4 14:18
     */
    ResultVo save(String url,String cookie,String content);

}