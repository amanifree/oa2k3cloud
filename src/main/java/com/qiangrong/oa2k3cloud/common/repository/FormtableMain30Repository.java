package com.qiangrong.oa2k3cloud.common.repository;

import com.qiangrong.oa2k3cloud.common.entity.FormtableMain30Entity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FormtableMain30Repository extends JpaRepository<FormtableMain30Entity,Integer>{

    //读取OA已归档未生单的差旅报销单
    @Query(nativeQuery = true, value = "select top(5)m.id,m.requestId,m.bianhao,m.baoxiaoriqi,m.chucdidian,m.suixingrenyuan,m.danjushu,m.chuchaishiyi,m.baoxiaogongsi,m.bxgsjt,i.name,m.baoxiaozjije from workflow_requestbase w " +
            " left join  formtable_main_30 m on w.requestid = m.requestId " +
            " left join mode_selectitempagedetail i on i.id = m.bxgsjt  " +
            " where w.workflowid=460 and w.currentnodetype=3 and w.status='归档'and i.mainid=1 " +
            " order by m.id ")
    public List<Object> findFormtableMain30List();

}
