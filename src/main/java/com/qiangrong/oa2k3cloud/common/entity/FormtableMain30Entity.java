package com.qiangrong.oa2k3cloud.common.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "formtable_main_30", schema = "dbo", catalog = "ecology")
public class FormtableMain30Entity {
    private int id;
    private Integer requestId;
    private String bianhao;
    private Integer baoxiaobumen;
    private Integer baoxiaoren;
    private String baoxiaoriqi;
    private Integer baoxiaodanwei;
    private String chucdidian;
    private String suixingrenyuan;
    private String danjushu;
    private BigDecimal yuzhichalvfei;
    private BigDecimal yingbufujine;
    private BigDecimal yingtuihuijine;
    private BigDecimal hejijinem2;
    private String fujian;
    private String beizhu;
    private Integer manager;
    private String bumenfuzheren;
    private String bumenfenguanlingdao;
    private String chuchaishiyi;
    private String qianzhishenpibiao;
    private BigDecimal hejichechuan;
    private BigDecimal hejizhusu;
    private BigDecimal hejihuoshi;
    private BigDecimal hejijiaotong;
    private BigDecimal hejiqita;
    private BigDecimal baoxiaozjije;
    private Integer shulhejifp;
    private BigDecimal hejitianshu;
    private String baoxiaogongsi;
    private Integer baoxiaogs;
    private Integer shenqrenjb;
    private Integer bxgsjt;
    private Integer bxgssy;
    private Integer bxgsjs;
    private String tittle;
    private Integer bxgssd;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "requestId", nullable = true)
    public Integer getRequestId() {
        return requestId;
    }

    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }

    @Basic
    @Column(name = "bianhao", nullable = true, length = 1000)
    public String getBianhao() {
        return bianhao;
    }

    public void setBianhao(String bianhao) {
        this.bianhao = bianhao;
    }

    @Basic
    @Column(name = "baoxiaobumen", nullable = true)
    public Integer getBaoxiaobumen() {
        return baoxiaobumen;
    }

    public void setBaoxiaobumen(Integer baoxiaobumen) {
        this.baoxiaobumen = baoxiaobumen;
    }

    @Basic
    @Column(name = "baoxiaoren", nullable = true)
    public Integer getBaoxiaoren() {
        return baoxiaoren;
    }

    public void setBaoxiaoren(Integer baoxiaoren) {
        this.baoxiaoren = baoxiaoren;
    }

    @Basic
    @Column(name = "baoxiaoriqi", nullable = true, length = 10,columnDefinition = "char")
    public String getBaoxiaoriqi() {
        return baoxiaoriqi;
    }

    public void setBaoxiaoriqi(String baoxiaoriqi) {
        this.baoxiaoriqi = baoxiaoriqi;
    }

    @Basic
    @Column(name = "baoxiaodanwei", nullable = true)
    public Integer getBaoxiaodanwei() {
        return baoxiaodanwei;
    }

    public void setBaoxiaodanwei(Integer baoxiaodanwei) {
        this.baoxiaodanwei = baoxiaodanwei;
    }

    @Basic
    @Column(name = "chucdidian", nullable = true, length = 1000)
    public String getChucdidian() {
        return chucdidian;
    }

    public void setChucdidian(String chucdidian) {
        this.chucdidian = chucdidian;
    }

    @Basic
    @Column(name = "suixingrenyuan", nullable = true, length = 1000)
    public String getSuixingrenyuan() {
        return suixingrenyuan;
    }

    public void setSuixingrenyuan(String suixingrenyuan) {
        this.suixingrenyuan = suixingrenyuan;
    }

    @Basic
    @Column(name = "danjushu", nullable = true, length = 1000)
    public String getDanjushu() {
        return danjushu;
    }

    public void setDanjushu(String danjushu) {
        this.danjushu = danjushu;
    }

    @Basic
    @Column(name = "yuzhichalvfei", nullable = true, precision = 2,columnDefinition = "decimal(15, 2)")
    public BigDecimal getYuzhichalvfei() {
        return yuzhichalvfei;
    }

    public void setYuzhichalvfei(BigDecimal yuzhichalvfei) {
        this.yuzhichalvfei = yuzhichalvfei;
    }

    @Basic
    @Column(name = "yingbufujine", nullable = true, precision = 2,columnDefinition = "decimal(15, 2)")
    public BigDecimal getYingbufujine() {
        return yingbufujine;
    }

    public void setYingbufujine(BigDecimal yingbufujine) {
        this.yingbufujine = yingbufujine;
    }

    @Basic
    @Column(name = "yingtuihuijine", nullable = true, precision = 2,columnDefinition = "decimal(15, 2)")
    public BigDecimal getYingtuihuijine() {
        return yingtuihuijine;
    }

    public void setYingtuihuijine(BigDecimal yingtuihuijine) {
        this.yingtuihuijine = yingtuihuijine;
    }

    @Basic
    @Column(name = "hejijinem2", nullable = true, precision = 2,columnDefinition = "decimal(15, 2)")
    public BigDecimal getHejijinem2() {
        return hejijinem2;
    }

    public void setHejijinem2(BigDecimal hejijinem2) {
        this.hejijinem2 = hejijinem2;
    }

    @Basic
    @Column(name = "fujian", nullable = true, length = -1,columnDefinition = "text")
    public String getFujian() {
        return fujian;
    }

    public void setFujian(String fujian) {
        this.fujian = fujian;
    }

    @Basic
    @Column(name = "beizhu", nullable = true, length = -1,columnDefinition = "text")
    public String getBeizhu() {
        return beizhu;
    }

    public void setBeizhu(String beizhu) {
        this.beizhu = beizhu;
    }

    @Basic
    @Column(name = "manager", nullable = true)
    public Integer getManager() {
        return manager;
    }

    public void setManager(Integer manager) {
        this.manager = manager;
    }

    @Basic
    @Column(name = "bumenfuzheren", nullable = true, length = 1000)
    public String getBumenfuzheren() {
        return bumenfuzheren;
    }

    public void setBumenfuzheren(String bumenfuzheren) {
        this.bumenfuzheren = bumenfuzheren;
    }

    @Basic
    @Column(name = "bumenfenguanlingdao", nullable = true, length = 1000)
    public String getBumenfenguanlingdao() {
        return bumenfenguanlingdao;
    }

    public void setBumenfenguanlingdao(String bumenfenguanlingdao) {
        this.bumenfenguanlingdao = bumenfenguanlingdao;
    }

    @Basic
    @Column(name = "chuchaishiyi", nullable = true, length = -1,columnDefinition = "text")
    public String getChuchaishiyi() {
        return chuchaishiyi;
    }

    public void setChuchaishiyi(String chuchaishiyi) {
        this.chuchaishiyi = chuchaishiyi;
    }

    @Basic
    @Column(name = "qianzhishenpibiao", nullable = true, length = -1,columnDefinition = "text")
    public String getQianzhishenpibiao() {
        return qianzhishenpibiao;
    }

    public void setQianzhishenpibiao(String qianzhishenpibiao) {
        this.qianzhishenpibiao = qianzhishenpibiao;
    }

    @Basic
    @Column(name = "hejichechuan", nullable = true, precision = 2,columnDefinition = "decimal(15, 2)")
    public BigDecimal getHejichechuan() {
        return hejichechuan;
    }

    public void setHejichechuan(BigDecimal hejichechuan) {
        this.hejichechuan = hejichechuan;
    }

    @Basic
    @Column(name = "hejizhusu", nullable = true, precision = 2,columnDefinition = "decimal(15, 2)")
    public BigDecimal getHejizhusu() {
        return hejizhusu;
    }

    public void setHejizhusu(BigDecimal hejizhusu) {
        this.hejizhusu = hejizhusu;
    }

    @Basic
    @Column(name = "hejihuoshi", nullable = true, precision = 2,columnDefinition = "decimal(15, 2)")
    public BigDecimal getHejihuoshi() {
        return hejihuoshi;
    }

    public void setHejihuoshi(BigDecimal hejihuoshi) {
        this.hejihuoshi = hejihuoshi;
    }

    @Basic
    @Column(name = "hejijiaotong", nullable = true, precision = 2,columnDefinition = "decimal(15, 2)")
    public BigDecimal getHejijiaotong() {
        return hejijiaotong;
    }

    public void setHejijiaotong(BigDecimal hejijiaotong) {
        this.hejijiaotong = hejijiaotong;
    }

    @Basic
    @Column(name = "hejiqita", nullable = true, precision = 2,columnDefinition = "decimal(15, 2)")
    public BigDecimal getHejiqita() {
        return hejiqita;
    }

    public void setHejiqita(BigDecimal hejiqita) {
        this.hejiqita = hejiqita;
    }

    @Basic
    @Column(name = "baoxiaozjije", nullable = true, precision = 2,columnDefinition = "decimal(15, 2)")
    public BigDecimal getBaoxiaozjije() {
        return baoxiaozjije;
    }

    public void setBaoxiaozjije(BigDecimal baoxiaozjije) {
        this.baoxiaozjije = baoxiaozjije;
    }

    @Basic
    @Column(name = "shulhejifp", nullable = true)
    public Integer getShulhejifp() {
        return shulhejifp;
    }

    public void setShulhejifp(Integer shulhejifp) {
        this.shulhejifp = shulhejifp;
    }

    @Basic
    @Column(name = "hejitianshu", nullable = true, precision = 1,columnDefinition = "decimal(15, 1)")
    public BigDecimal getHejitianshu() {
        return hejitianshu;
    }

    public void setHejitianshu(BigDecimal hejitianshu) {
        this.hejitianshu = hejitianshu;
    }

    @Basic
    @Column(name = "baoxiaogongsi", nullable = true, length = 1000)
    public String getBaoxiaogongsi() {
        return baoxiaogongsi;
    }

    public void setBaoxiaogongsi(String baoxiaogongsi) {
        this.baoxiaogongsi = baoxiaogongsi;
    }

    @Basic
    @Column(name = "baoxiaogs", nullable = true)
    public Integer getBaoxiaogs() {
        return baoxiaogs;
    }

    public void setBaoxiaogs(Integer baoxiaogs) {
        this.baoxiaogs = baoxiaogs;
    }

    @Basic
    @Column(name = "shenqrenjb", nullable = true)
    public Integer getShenqrenjb() {
        return shenqrenjb;
    }

    public void setShenqrenjb(Integer shenqrenjb) {
        this.shenqrenjb = shenqrenjb;
    }

    @Basic
    @Column(name = "bxgsjt", nullable = true)
    public Integer getBxgsjt() {
        return bxgsjt;
    }

    public void setBxgsjt(Integer bxgsjt) {
        this.bxgsjt = bxgsjt;
    }

    @Basic
    @Column(name = "bxgssy", nullable = true)
    public Integer getBxgssy() {
        return bxgssy;
    }

    public void setBxgssy(Integer bxgssy) {
        this.bxgssy = bxgssy;
    }

    @Basic
    @Column(name = "bxgsjs", nullable = true)
    public Integer getBxgsjs() {
        return bxgsjs;
    }

    public void setBxgsjs(Integer bxgsjs) {
        this.bxgsjs = bxgsjs;
    }

    @Basic
    @Column(name = "tittle", nullable = true, length = 50)
    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    @Basic
    @Column(name = "bxgssd", nullable = true)
    public Integer getBxgssd() {
        return bxgssd;
    }

    public void setBxgssd(Integer bxgssd) {
        this.bxgssd = bxgssd;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FormtableMain30Entity that = (FormtableMain30Entity) o;
        return id == that.id &&
                Objects.equals(requestId, that.requestId) &&
                Objects.equals(bianhao, that.bianhao) &&
                Objects.equals(baoxiaobumen, that.baoxiaobumen) &&
                Objects.equals(baoxiaoren, that.baoxiaoren) &&
                Objects.equals(baoxiaoriqi, that.baoxiaoriqi) &&
                Objects.equals(baoxiaodanwei, that.baoxiaodanwei) &&
                Objects.equals(chucdidian, that.chucdidian) &&
                Objects.equals(suixingrenyuan, that.suixingrenyuan) &&
                Objects.equals(danjushu, that.danjushu) &&
                Objects.equals(yuzhichalvfei, that.yuzhichalvfei) &&
                Objects.equals(yingbufujine, that.yingbufujine) &&
                Objects.equals(yingtuihuijine, that.yingtuihuijine) &&
                Objects.equals(hejijinem2, that.hejijinem2) &&
                Objects.equals(fujian, that.fujian) &&
                Objects.equals(beizhu, that.beizhu) &&
                Objects.equals(manager, that.manager) &&
                Objects.equals(bumenfuzheren, that.bumenfuzheren) &&
                Objects.equals(bumenfenguanlingdao, that.bumenfenguanlingdao) &&
                Objects.equals(chuchaishiyi, that.chuchaishiyi) &&
                Objects.equals(qianzhishenpibiao, that.qianzhishenpibiao) &&
                Objects.equals(hejichechuan, that.hejichechuan) &&
                Objects.equals(hejizhusu, that.hejizhusu) &&
                Objects.equals(hejihuoshi, that.hejihuoshi) &&
                Objects.equals(hejijiaotong, that.hejijiaotong) &&
                Objects.equals(hejiqita, that.hejiqita) &&
                Objects.equals(baoxiaozjije, that.baoxiaozjije) &&
                Objects.equals(shulhejifp, that.shulhejifp) &&
                Objects.equals(hejitianshu, that.hejitianshu) &&
                Objects.equals(baoxiaogongsi, that.baoxiaogongsi) &&
                Objects.equals(baoxiaogs, that.baoxiaogs) &&
                Objects.equals(shenqrenjb, that.shenqrenjb) &&
                Objects.equals(bxgsjt, that.bxgsjt) &&
                Objects.equals(bxgssy, that.bxgssy) &&
                Objects.equals(bxgsjs, that.bxgsjs) &&
                Objects.equals(tittle, that.tittle) &&
                Objects.equals(bxgssd, that.bxgssd);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, requestId, bianhao, baoxiaobumen, baoxiaoren, baoxiaoriqi, baoxiaodanwei, chucdidian, suixingrenyuan, danjushu, yuzhichalvfei, yingbufujine, yingtuihuijine, hejijinem2, fujian, beizhu, manager, bumenfuzheren, bumenfenguanlingdao, chuchaishiyi, qianzhishenpibiao, hejichechuan, hejizhusu, hejihuoshi, hejijiaotong, hejiqita, baoxiaozjije, shulhejifp, hejitianshu, baoxiaogongsi, baoxiaogs, shenqrenjb, bxgsjt, bxgssy, bxgsjs, tittle, bxgssd);
    }
}
