package com.qiangrong.oa2k3cloud.common.utils.result;

import com.qiangrong.oa2k3cloud.common.vo.ResultVo;

import java.util.Map;

public class ResultUtil {
    private static ResultVo resultVo = new ResultVo();
    public static ResultVo success() {
        resultVo.setCode("SUCCESS");
        resultVo.setData("");
        return resultVo;
    }

    public static ResultVo error(String toJSONString) {
        resultVo.setCode("ERROR");
        resultVo.setMsg(toJSONString);
        return resultVo;
    }

    public static ResultVo success(Map<String, Object> map) {
        resultVo.setCode("SUCCESS");
        resultVo.setData(map);
        return resultVo;
    }
}
