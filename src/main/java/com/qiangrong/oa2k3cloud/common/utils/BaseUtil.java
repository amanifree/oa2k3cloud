package com.qiangrong.oa2k3cloud.common.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * @date 2019/1/7 15:58
 */
public class BaseUtil {

    private BaseUtil(){};

    /**
     * 构造登录参数
     * @param dbid 账套id
     * @param userName 用户名
     * @param password 密码
     * @param lang 语言
     * @return
     */
    public static String buildLogin(String dbid, String userName, String password, int lang){
        Map<String,Object> param = new HashMap<>(4);
        param.put("acctID",dbid);
        param.put("username",userName);
        param.put("password",password);
        param.put("lcid",lang);
        return JSON.toJSONString(param);
    }


    /**
     * 初始化物料信息
     * @param template 物料基础数据模板
     * @param formid 表单id
     * @param code 物料编码
     * @param name 物料名称
     * @param attr 物料规格属性
     * @return
     */
    public static String buildMaterial(String template,String formid,String code,String name,String attr){
        JSONObject basic = JSON.parseObject(template);
        Map<String,Object> model = (Map<String, Object>) basic.get("Model");
        /*Map<String,Object> model1 = (Map<String, Object>) model.get("SubHeadEntity4");
        model1.put("FFixLeadTime",2);
        model.put("SubHeadEntity4",model1);*/
        model.put("FNumber",code);
        model.put("FName",name);
        model.put("FSpecification",attr);
        basic.put("Model",model);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("formid",formid);
        jsonObject.put("data",JSON.toJSONString(basic));
        return JSON.toJSONString(jsonObject);
    }

    /**
     * 构造提交、审核参数
     * @param formid 表单id
     * @param numbers 编码 多个编码以,分隔
     * @param flags 审核标示 多个以,分隔 和编码一一对应
     * @return
     */
    public static String buildParam(String formid,String numbers,String flags){
        JSONObject jsonObject = new JSONObject();
        JSONObject param = new JSONObject();
        if(flags!=null){
            String[] arr_flag = flags.split(",");
            param.put("InterationFlags",arr_flag);
        }
        String[] arr_number = numbers.split(",");
        param.put("Numbers",arr_number);
        jsonObject.put("formid",formid);
        jsonObject.put("data",JSON.toJSONString(param));
        return JSON.toJSONString(jsonObject);
    }

    /**
     * 初始化会计凭证信息
     * @param template 会计凭证数据模板
     * @param formid 表单id
     * @param fabookid 账簿编号
     * @param fdate 凭证日期
     * @param zhaiyao 摘要
     * @param kemu1 分录1科目
     * @param money1 分录1金额
     * @param kemu2 分录2科目
     * @param money2 分录2金额
     * @return
     */
    public static String buildVoucher(String template,String formid,String fabookid,String fdate,String zhaiyao,String kemu1,Double money1,String kemu2,Double money2){

        JSONObject basic = JSON.parseObject(template);
        Map<String,Object> model = (Map<String, Object>) basic.get("Model");
        //设置核算账簿
        Map<String,Object> fabook = (Map<String, Object>) model.get("FAccountBookID");
        fabook.put("FNumber",fabookid);
        model.put("FAccountBookID",fabook);
        //设置凭证日期
        model.put("FDate",fdate);
        //设置凭证摘要、借贷及金额
        JSONArray jsonArray = (JSONArray) model.get("FEntity");
        //分录1摘要、借方金额
        Map<String,Object> fenlu1 = jsonArray.getJSONObject(0);
        fenlu1.put("FEXPLANATION",zhaiyao);
        fenlu1.put("FAMOUNTFOR",money1);
        fenlu1.put("FDEBIT",money1);
        //分录1科目
        Map<String,Object> flkemu1 = (Map<String, Object>) fenlu1.get("FACCOUNTID");
        flkemu1.put("FNumber",kemu1);
        fenlu1.put("FACCOUNTID",flkemu1);
        //jsonArray修改后是否需要放回？
        //分录2摘要、贷方金额
        Map<String,Object> fenlu2 = jsonArray.getJSONObject(1);
        fenlu2.put("FEXPLANATION",zhaiyao);
        fenlu2.put("FAMOUNTFOR",money2);
        fenlu2.put("FCREDIT",money2);
        //分录2科目
        Map<String,Object> flkemu2 = (Map<String, Object>) fenlu2.get("FACCOUNTID");
        flkemu2.put("FNumber",kemu2);
        fenlu2.put("FACCOUNTID",flkemu2);

        basic.put("Model",model);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("formid",formid);
        jsonObject.put("data",JSON.toJSONString(basic));
        return JSON.toJSONString(jsonObject);
    }

    public static JSONObject getFaBookId(String template){
        JSONObject fabook = JSON.parseObject(template);
        return fabook;
        /*String orgid = (String) fabook.get(orgName);
        return orgid;*/
    }

}
