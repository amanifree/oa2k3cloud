package com.qiangrong.oa2k3cloud.common.service.impl;

import com.qiangrong.oa2k3cloud.common.repository.FormtableMain30Repository;
import com.qiangrong.oa2k3cloud.common.service.FormtableMain30Management;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FormtableMain30ManagementImpl implements FormtableMain30Management {

    @Autowired
    private FormtableMain30Repository formtableMain30Repository;


    public List<Object> findFormtableMain30List(){
        List<Object> m30list = new ArrayList();
        m30list = formtableMain30Repository.findFormtableMain30List();
        return m30list;
    }


}
