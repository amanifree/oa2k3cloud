package com.qiangrong.oa2k3cloud.common.service;

import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface FormtableMain30Management {
    public List<Object> findFormtableMain30List();
}
