package com.qiangrong.oa2k3cloud.common.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


@Data
@ConfigurationProperties(prefix = "kingdee")
@Component
public class K3CloundConfig {

    /**
     * 金蝶url前缀
     */
    private String url;

    /**
     * 登录接口
     */
    private String login;

    /**
     * 查看接口
     */
    private String view;

    /**
     * 保存接口
     */
    private String save;

    /**
     * 提交接口
     */
    private String submit;


    /**
     * 审核接口
     */
    private String audit;

    /**
     * 状态改变接口
     */
    private String statusConver;



}

