package com.qiangrong.oa2k3cloud.common.vo;

import lombok.Data;

@Data
public class ResultVo {
    public Object code,msg,data;
}
