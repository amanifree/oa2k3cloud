package com.qiangrong.oa2k3cloud.common.enums;


import lombok.Getter;

@Getter
public enum ResultEnum {
    SUCCESS("SUCCESS"),
    ERROR("ERROR");

    private String code;
    ResultEnum(String code) {
        this.code = code;
    }
}
