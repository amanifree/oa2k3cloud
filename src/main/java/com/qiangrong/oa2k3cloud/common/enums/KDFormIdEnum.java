package com.qiangrong.oa2k3cloud.common.enums;

import lombok.Getter;


@Getter
public enum  KDFormIdEnum {

    MATERIAL("BD_MATERIAL"),//物料
    OUT_STOCK("SAL_OUTSTOCK"),//出库单
    STOCK("BD_STOCK"),//仓库
    CUSTOMER("BD_Customer"),//客户
    GL_VOUCHER("GL_VOUCHER");//会计凭证

    private String formid;

    KDFormIdEnum(String formid) {
        this.formid = formid;
    }
}