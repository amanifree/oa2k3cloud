package com.qiangrong.oa2k3cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableScheduling
@SpringBootApplication
public class Oa2k3cloudApplication {
      @RequestMapping("/")
      public String first(){return "OA2K3Cloud";}

    public static void main(String[] args) {
        SpringApplication.run(Oa2k3cloudApplication.class, args);
    }

}
